#include "droneOuts.h"

using namespace std;


////// RotationAngles ////////
RotationAnglesROSModule::RotationAnglesROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

RotationAnglesROSModule::~RotationAnglesROSModule()
{

    return;
}

void RotationAnglesROSModule::init()
{
  return;
}

void RotationAnglesROSModule::close()
{
   return;
}

void RotationAnglesROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    RotationAnglesPubl = n.advertise<geometry_msgs::Vector3Stamped>("rotation_angles", 1, true);
    ImuDataPubl        = n.advertise<sensor_msgs::Imu>("imu",1,true);
    RotationAnglesRadiansPub = n.advertise<geometry_msgs::Vector3Stamped>("rotation_angles_radians",1, true);

    //Subscriber
    RotationAnglesSubs=n.subscribe("mavros/imu/data", 1, &RotationAnglesROSModule::rotationAnglesCallback, this);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //Updating lastTime
    this->lastTimeRotation = ros::Time::now();

    //End
    return;
}

//Reset
bool RotationAnglesROSModule::resetValues()
{
    return true;
}

//Start
bool RotationAnglesROSModule::startVal()
{
    return true;
}

//Stop
bool RotationAnglesROSModule::stopVal()
{
    return true;
}

//Run
bool RotationAnglesROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void RotationAnglesROSModule::rotationAnglesCallback(const sensor_msgs::Imu &msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //deg,   mavwork reference frame
    geometry_msgs::Vector3Stamped RotationAnglesMsgs;
    geometry_msgs::Vector3Stamped RotationAnglesRadiansMsgs;

    RotationAnglesMsgs.header.stamp         = ros::Time::now();
    RotationAnglesRadiansMsgs.header.stamp  = ros::Time::now();

    //convert quaternion msg to eigen
    tf::quaternionMsgToEigen(msg.orientation, quaterniond);

//------ The below code converts ENU (Mavros) frame to NED (Aerostack) frame ------- ///

    //Rotating the frame in x-axis by 180 degrees
    Eigen::Quaterniond BASE_LINK_TO_AIRCRAFT = mavros::UAS::quaternion_from_rpy(M_PI, 0.0, 0.0);
    quaterniond = quaterniond*BASE_LINK_TO_AIRCRAFT;

    //Rotating the frame in x-axis by 180 deg and in z-axis by 90 axis (accordance to the new mavros update)
    Eigen::Quaterniond ENU_TO_NED = mavros::UAS::quaternion_from_rpy(M_PI, 0.0, M_PI_2);
    quaterniond = ENU_TO_NED*quaterniond;
//-----------------------------------------------------------------------------------///

    //converting back quaternion from eigen to msg
    tf::quaternionEigenToMsg(quaterniond, quaternion);

    tf::Quaternion q(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
    tf::Matrix3x3 m(q);

    mavros::UAS::quaternion_to_rpy(quaterniond, roll, pitch, yaw);

   //convert quaternion to euler angels
    m.getEulerYPR(yaw, pitch, roll);

    RotationAnglesRadiansMsgs.vector.x =  roll;
    RotationAnglesRadiansMsgs.vector.y =  pitch;
    RotationAnglesRadiansMsgs.vector.z = -yaw;

    RotationAnglesMsgs.vector.x = (double)(+1)*(roll)*180/M_PI;
    RotationAnglesMsgs.vector.y = (double)(+1)*(pitch)*180/M_PI;
    RotationAnglesMsgs.vector.z = (double)(+1)*(yaw)*180/M_PI;


    //converting the yaw which is in the range from -phi to +phi to 0 to 360 degrees
    if( RotationAnglesMsgs.vector.z < 0 )
        RotationAnglesMsgs.vector.z += 360.0;

    publishRotationAnglesValue(RotationAnglesMsgs);
    publishImuData(msg);
    RotationAnglesRadiansPub.publish(RotationAnglesRadiansMsgs);
    return;
}

bool RotationAnglesROSModule::publishRotationAnglesValue(const geometry_msgs::Vector3Stamped &RotationAnglesMsgs)
{
    if(droneModuleOpened==false)
        return false;

    RotationAnglesPubl.publish(RotationAnglesMsgs);
    return true;
}

bool RotationAnglesROSModule::publishImuData(const sensor_msgs::Imu &ImuData)
{
   if(droneModuleOpened==false)
      return false;

   sensor_msgs::Imu ImuMsg;

   ImuMsg = ImuData;

   //converting the acceleration and velocity in Mavframe (NED frame)
   ImuMsg.linear_acceleration.y*=-1;
   ImuMsg.linear_acceleration.z*=-1;

   ImuMsg.angular_velocity.y*=-1;
   ImuMsg.angular_velocity.z*=-1;

   ImuDataPubl.publish(ImuMsg);
   return true;

}

////// Battery ////////
BatteryROSModule::BatteryROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

BatteryROSModule::~BatteryROSModule()
{

    return;
}

void BatteryROSModule::init()
{
  return;
}

void BatteryROSModule::close()
{
   return;
}

void BatteryROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    BatteryPubl = n.advertise<droneMsgsROS::battery>("battery", 1, true);


    //Subscriber
    BatterySubs=n.subscribe("mavros/battery", 1, &BatteryROSModule::batteryCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool BatteryROSModule::resetValues()
{
    return true;
}

//Start
bool BatteryROSModule::startVal()
{
    return true;
}

//Stop
bool BatteryROSModule::stopVal()
{
    return true;
}

//Run
bool BatteryROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void BatteryROSModule::batteryCallback(const mavros_msgs::BatteryStatus &msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    droneMsgsROS::battery BatteryMsgs;

    // Read Battery from navdata
    BatteryMsgs.header   = msg.header;
    // minimum voltage per cell used by the "digital battery checker" is 3.55 V
    // msg.voltage is in V
    float battery_voltage = (msg.voltage);
    BatteryMsgs.batteryPercent = ((battery_voltage - 14.8)/(16.8-14.8)) * 100;

    publishBatteryValue(BatteryMsgs);
    return;
}


bool BatteryROSModule::publishBatteryValue(const droneMsgsROS::battery &BatteryMsgs)
{
    if(droneModuleOpened==false)
        return false;

    BatteryPubl.publish(BatteryMsgs);
    return true;
}

////// Altitude ////////
AltitudeROSModule::AltitudeROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

AltitudeROSModule::~AltitudeROSModule()
{

    return;
}

void AltitudeROSModule::init()
{


    filtered_derivative_wcb.setTimeParameters( 0.005,0.005,0.200,1.0,100.000);
    filtered_derivative_wcb.reset();

  return;
}

void AltitudeROSModule::close()
{
   return;
}

void AltitudeROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    init();


    //Configuration


    //Publisher
    AltitudePub          = n.advertise<droneMsgsROS::droneAltitude>("altitude", 1, true);


    //Subscriber
#ifdef NoAltitudeFiltering
    AltitudeSub          = n.subscribe("lightware_altitude", 1, &AltitudeROSModule::publishAlitudeCallback, this);
#endif

#ifdef AltitudeFiltering
    AltitudeFilteredSub  = n.subscribe("altitudeFiltered",1,&AltitudeROSModule::publishAltitudeFilteredCallback, this);
#endif

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool AltitudeROSModule::resetValues()
{
    return true;
}

//Start
bool AltitudeROSModule::startVal()
{
    return true;
}

//Stop
bool AltitudeROSModule::stopVal()
{
    return true;
}

//Run
bool AltitudeROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

#ifdef NoAltitudeFiltering
void AltitudeROSModule::publishAlitudeCallback(const sensor_msgs::Range &msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;


    ros::Time current_timestamp = ros::Time::now();

    double zraw_t = (-1.0) * msg.range;

    time_t tv_sec; suseconds_t tv_usec;
    {
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb.setInput( zraw_t, tv_sec, tv_usec);
    }

    double z_t, dz_t;
    filtered_derivative_wcb.getOutput( z_t, dz_t);


    //Read Altitude from Pose
    AltitudeMsg.header = msg.header;
    // correct  timestamp
    AltitudeMsg.header.stamp  = current_timestamp;
    //Altitude needs to be put in [m], mavwork reference frame!!
    AltitudeMsg.altitude = z_t;  // m
    AltitudeMsg.var_altitude   = 0.0;
    // [m/s], mavwork reference frame
    AltitudeMsg.altitude_speed = dz_t;
    AltitudeMsg.var_altitude_speed = 0.0;


    publishAltitude(AltitudeMsg);

    return;
}
#endif

#ifdef AltitudeFiltering
void AltitudeROSModule::publishAltitudeFilteredCallback(const geometry_msgs::PoseStamped &msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;


    ros::Time current_timestamp = ros::Time::now();

    double zraw_t = (-1.0) * msg.pose.position.z;

    time_t tv_sec; suseconds_t tv_usec;
    {
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb.setInput( zraw_t, tv_sec, tv_usec);
    }

    double z_t, dz_t;
    filtered_derivative_wcb.getOutput( z_t, dz_t);


    //Read Altitude from Pose
    AltitudeMsg.header = msg.header;
    // correct  timestamp
    AltitudeMsg.header.stamp  = current_timestamp;
    //Altitude needs to be put in [m], mavwork reference frame!!
    AltitudeMsg.altitude = z_t;  // m
    AltitudeMsg.var_altitude   = 0.0;
    // [m/s], mavwork reference frame
    AltitudeMsg.altitude_speed = dz_t;
    AltitudeMsg.var_altitude_speed = 0.0;


    publishAltitude(AltitudeMsg);

    return;
}

#endif


bool AltitudeROSModule::publishAltitude(const droneMsgsROS::droneAltitude altitudemsg)
{
    if(droneModuleOpened==false)
        return false;

     AltitudePub.publish(altitudemsg);

    return true;
}



////// Local Pose from mavros ////////
LocaPoseROSModule::LocaPoseROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

LocaPoseROSModule::~LocaPoseROSModule()
{

    return;
}

void LocaPoseROSModule::init()
{

    filtered_derivative_wcb.setTimeParameters(0.02,0.02,0.200,1.0,100.000);
    filtered_derivative_wcb.reset();

    return;
}

void LocaPoseROSModule::close()
{
   return;
}

void LocaPoseROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    init();


    //Configuration


    //Publishers
   localPoseAltitudePublisher  = n.advertise<droneMsgsROS::droneAltitude>("altitude", 1, true);
   localPoseSpeedsPublisher    = n.advertise<droneMsgsROS::vector2Stamped>("ground_speed",1, true);
   localPosePublisher          = n.advertise<droneMsgsROS::dronePose>("gazebo_estimated_pose",1, true);
   localSpeedPublisher         = n.advertise<droneMsgsROS::droneSpeeds>("gazebo_estimated_speed",1, true);


   /* Ground truth subscribers */
     mavrosLocalSpeedsSubsriber = n.subscribe("/gazebo/model_states",1, &LocaPoseROSModule::localSpeedsCallbackGazebo, this);

    /* Ground truth subscribers */
 #ifdef NoAltitudeFiltering
     mavrosLocalPoseSubsriber   = n.subscribe("/px4flow/raw/px4Flow_pose_z", 1, &LocaPoseROSModule::localPoseAltitudeCallback, this);
 #endif

 #ifdef AltitudeFiltering
     mavrosLocalPoseSubsriber   = n.subscribe("altitudeFiltered", 1, &LocaPoseROSModule::localPoseAltitudeCallback, this);
 #endif


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool LocaPoseROSModule::resetValues()
{
    return true;
}

//Start
bool LocaPoseROSModule::startVal()
{
    return true;
}

//Stop
bool LocaPoseROSModule::stopVal()
{
    return true;
}

//Run
bool LocaPoseROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void LocaPoseROSModule::localPoseAltitudeCallback(const geometry_msgs::PoseStamped &msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    ros::Time current_timestamp = ros::Time::now();

    double zraw_t = (-1.0) * msg.pose.position.z;

    time_t tv_sec; suseconds_t tv_usec;
    {
    tv_sec  = current_timestamp.sec;
    tv_usec = current_timestamp.nsec / 1000.0;
    filtered_derivative_wcb.setInput( zraw_t, tv_sec, tv_usec);
    }

    double z_t, dz_t;
    filtered_derivative_wcb.getOutput( z_t, dz_t);


    //Read Altitude from Pose
    AltitudeMsg.header = msg.header;
    // correct  timestamp
    AltitudeMsg.header.stamp  = current_timestamp;
    //Altitude needs to be put in [m], mavwork reference frame!!
    AltitudeMsg.altitude = z_t;  // m
    AltitudeMsg.var_altitude   = 0.0;
    // [m/s], mavwork reference frame
    AltitudeMsg.altitude_speed = dz_t;
    AltitudeMsg.var_altitude_speed = 0.0;


    publishLocalPoseAltitude(AltitudeMsg);

    return;
}

void LocaPoseROSModule::localSpeedsCallbackGazebo(const gazebo_msgs::ModelStatesConstPtr &msg)
{
      SpeedsMsg.header.stamp = ros::Time::now();

      /* Calculating Roll, Pitch, Yaw */
      tf::Quaternion q(msg->pose[2].orientation.x, msg->pose[2].orientation.y, msg->pose[2].orientation.z, msg->pose[2].orientation.w);
      tf::Matrix3x3 m(q);

      //convert quaternion to euler angels
      double y, p, r;
      m.getEulerYPR(y, p, r);

      /* Rotation from Global frame to body frame */
      Eigen::Vector3f BodyFrame;
      Eigen::Vector3f GlobalFrame;
      Eigen::Matrix3f RotationMat;

      GlobalFrame(0,0) = (+1)*msg->twist[2].linear.x;
      GlobalFrame(1,0) = (+1)*msg->twist[2].linear.y;
      GlobalFrame(2,0) = 0;

      RotationMat(0,0) = cos(y);
      RotationMat(1,0) = -sin(y);
      RotationMat(2,0) = 0;

      RotationMat(0,1) = sin(y);
      RotationMat(1,1) = cos(y);
      RotationMat(2,1) = 0;

      RotationMat(0,2) = 0;
      RotationMat(1,2) = 0;
      RotationMat(2,2) = 1;

      BodyFrame = RotationMat*GlobalFrame;

      SpeedsMsg.vector.x = (+1) * BodyFrame(0,0);
      SpeedsMsg.vector.y = (-1) * BodyFrame(1,0);

      gazeboPoseMsg.x = msg->pose[2].position.x;
      gazeboPoseMsg.y = msg->pose[2].position.y;
      gazeboPoseMsg.z = msg->pose[2].position.z;
      gazeboPoseMsg.roll = r;
      gazeboPoseMsg.pitch = p;
      gazeboPoseMsg.yaw = y;

      gazeboSpeedsMsg.dx = msg->twist[2].linear.x;
      gazeboSpeedsMsg.dy = msg->twist[2].linear.y;
      gazeboSpeedsMsg.dz = msg->twist[2].linear.z;



      publishLocalPoseSpeeds(SpeedsMsg);
      localPosePublisher.publish(gazeboPoseMsg);
      localSpeedPublisher.publish(gazeboSpeedsMsg);

      return;
}

void LocaPoseROSModule::localSpeedsCallback(const geometry_msgs::TwistStamped &msg)
{
      SpeedsMsg.header = msg.header;

      SpeedsMsg.vector.x = (+1)*msg.twist.linear.x;
      SpeedsMsg.vector.y = (+1)*msg.twist.linear.y;

      publishLocalPoseSpeeds(SpeedsMsg);
      return;
}
bool LocaPoseROSModule::publishLocalPoseAltitude(const droneMsgsROS::droneAltitude &altitudemsg)
{
    if(droneModuleOpened==false)
        return false;

    localPoseAltitudePublisher.publish(altitudemsg);
    return true;

}

bool LocaPoseROSModule::publishLocalPoseSpeeds(const droneMsgsROS::vector2Stamped &speedsmsg)
{
  if(droneModuleOpened==false)
      return false;

  localPoseSpeedsPublisher.publish(speedsmsg);
  return true;
}
